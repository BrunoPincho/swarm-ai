use tcod::colors::*;
use tcod::console::*;
use tcod::input::KeyCode::*;
use rand::Rng;
use std::collections::HashMap;

const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;

const LIMIT_FPS: i32 = 20;

#[derive(PartialEq, Eq, Hash)]
enum State {
  Zero,
  FreeFlight,
  Pursue,
  Intercept,
  Rogue
}

#[derive(Debug)]
struct PointerBox<T> {
    pointer: *const T
}

struct GridManager {
    grid: std::vec::Vec<std::vec::Vec<PointerBox<Bee>>>,
    init_bee: Bee
}

struct Tcod {
  root: Root,
  con: Offscreen
}

#[allow(dead_code)]
struct Hive {
	active_bees: u16,
	onlooker_bees: u16,
	employed_bees: u16,
	scout_bees: u16,
	best_food_source: (u32, u32),
    swarm: Vec<Bee>
}

#[allow(dead_code)]
struct Behavior {
    movement: HashMap<State, Box <dyn Fn(& Bee) -> (i32, i32)>>,
    color: HashMap<State, Color>,
    direction: HashMap<State, String>
}

#[allow(dead_code)]
struct Bee {
    x: i32,
    y: i32,
    char: char,
    state: State,
    behavior: Behavior,
    slope_coeff: f32
}

impl GridManager {
    pub fn new(bee_list: &Vec<Bee>) -> Self {
        let init_bee = Bee {
            x: -1 as i32,
            y: -1 as i32,
            char: '0',
            state: State::FreeFlight,
            behavior: Behavior {
                movement: HashMap::new(),
                color: HashMap::new(),
                direction: HashMap::new()
            },
            slope_coeff: 0.0
        };

        let mut grid = Vec::new();
        for _i in 0..81 {
            let mut line = Vec::new();
            for _j in 0..51 {
                line.push(  PointerBox{ pointer: &init_bee });
            }
            grid.push(line);
        }
        for bee in bee_list.iter() {
            let (x,y) = bee.location();
            println!("{:?},{:?}",x,y );
            grid[x as usize][y as usize].pointer = bee;
        }
        GridManager { grid: grid, init_bee }
    }

    pub fn print_grid(&self) {
        for x in 0..80{
            for y in 0..50 {
                let coords = unsafe {(&*self.grid[x][y].pointer).location()};
                if coords.0 > 0 && coords.1 > 0 {
                    println!("x:{} y:{}", coords.0, coords.1);
                }
            }
        }
    }

    pub fn clear_cell(&mut self, x: usize, y: usize) {
        self.grid[x][y].pointer = &self.init_bee;
    }

    pub fn insert_cell(&mut self, x: usize, y: usize, bee: &Bee) {
        self.grid[x][y].pointer = bee;
    }

    pub fn update_pos(&mut self, old_x: i32, old_y: i32, new_x: i32, new_y:i32, bee: &Bee) {
        self.clear_cell(old_x as usize, old_y as usize);
        self.insert_cell(new_x as usize, new_y as usize, bee)
    }

}

impl Bee{
    pub fn new(x: i32, y: i32, placehold: char) -> Self {
        let mut movement: HashMap<State, Box <dyn Fn(& Self) -> (i32, i32)>> = HashMap::new();
        let mut color_hash = HashMap::new();
        let mut direction = HashMap::new();

        movement.insert(State::Pursue, Box::new(Bee::pursue_walk));
        movement.insert(State::Intercept, Box::new(Bee::_intercept_walk));
        movement.insert(State::FreeFlight, Box::new(Bee::random_walk_frame));
        movement.insert(State::Rogue, Box::new(Bee::rogue_walk));

        color_hash.insert(State::FreeFlight, YELLOW);
        color_hash.insert(State::Pursue, RED);
        color_hash.insert(State::Intercept, BLUE);
        color_hash.insert(State::Rogue, ORANGE);

        direction.insert(State::FreeFlight, String::from("y=mx"));
        direction.insert(State::Pursue, String::from("y=mx"));
        direction.insert(State::Intercept, String::from("y=mx"));
        direction.insert(State::Rogue, String::from("y=mx"));

        let behavior = Behavior{movement, color: color_hash, direction};

        let new_b = Bee {x, y, char: placehold, state: State::FreeFlight, behavior, slope_coeff: 0.0};

        new_b
    }

    pub fn set_state(&mut self, new_state: State) {
      self.state = new_state;
    }

    pub fn move_to(&mut self, new_x: i32, new_y: i32) {
        self.x = new_x;
        self.y = new_y;
    }

    pub fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.behavior.color[&self.state]);
        con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
    }

    pub fn location(&self) -> (i32, i32) {
      (self.x, self.y)
    }

    pub fn movement(&mut self, grid: &mut GridManager) {
      let new_position = self.behavior.movement[&self.state](self);
      grid.update_pos(self.x, self.y, new_position.0, new_position.1, &self);
      self.move_to(new_position.0, new_position.1);
    }

    fn pursue_walk(&self) -> (i32, i32) {
      self.random_walk_frame()
    }

    fn rogue_walk(&self) -> (i32, i32) {
        if self.y == SCREEN_HEIGHT {
            return (self.x, 0)
        } else if self.y >= 0 {
            return (self.x, self.y + 1)
        } else {
            (self.x, self.y)
        }
    }

    pub fn _intercept_walk(&self) -> (i32, i32) {
      (0,0)
    }

    pub fn random_walk_frame(&self) -> (i32, i32) {
      let mut rng = rand::thread_rng();
      let mut tmp_dy = -1;
      let mut tmp_dx = -1;
      let mut dy;
      let mut dx;

      while tmp_dx < 0 || tmp_dx > SCREEN_WIDTH -1 {
        dx = rng.gen_range(-1, 2);
        tmp_dx = self.x + dx;
      }

      while tmp_dy < 0 || tmp_dy > SCREEN_HEIGHT -1 {
        dy = rng.gen_range(-1, 2);
        tmp_dy = self.y + dy;
      }
      // self.move_to(tmp_dx, tmp_dy);
      (tmp_dx, tmp_dy)
    }

    pub fn linear_regression(&mut self, dataset: Vec<(f32, f32)>) {
        // deviant_sum = sum[i_to_n] (xi - x_avg)*(yi - y_avg)
        // b1 = deviant_sum/squared_x_deviation
        let dataset_size = dataset.len() as f32;
        let averages = {
            let mut avr_x: f32 = 0.0;
            let mut avr_y: f32 = 0.0;
            for coord in dataset.iter(){
                avr_x += coord.0;
                avr_y += coord.1;
            }
            (avr_x/dataset_size, avr_y/dataset_size)
        };
        let deviant_sum = {
            let mut temp_sum: f32 = 0.0;
            for coord in dataset.iter(){
                temp_sum += (coord.0 - averages.0)*(coord.1 - averages.1);
            }
            temp_sum
        };
        let squared_x_deviation = {
            let mut temp_sum: f32 = 0.0;
            for coord in dataset.iter(){
                temp_sum += (coord.0 - averages.0).powf(2.0);
            }
            temp_sum
        };
        self.slope_coeff = {
            deviant_sum / squared_x_deviation
        };
    }
}

impl Hive{

  pub fn new(active_bees: u16, onlooker_bees: u16, employed_bees: u16, scout_bees: u16, best_food_source: (u32, u32)) -> Self {
    let total_bees = (active_bees + onlooker_bees + employed_bees + scout_bees) as usize;
    Hive { active_bees, onlooker_bees, employed_bees, scout_bees, best_food_source, swarm: Hive::build_swarm(total_bees) }
  }

	fn get_colony_size(&self) {
		println!("colony of size:{}\n",self.onlooker_bees + self.employed_bees + self.scout_bees);
	}

	fn get_best_food(&self) {
		println!("best food source located at x: {}, y:{}\n", self.best_food_source.0, self.best_food_source.1)
	}

  pub fn attack_swarm(&mut self) {
    for bee in self.swarm.iter_mut(){
      bee.set_state(State::Pursue);
    }
  }

  pub fn ease_swarm(&mut self) {
    for bee in self.swarm.iter_mut(){
      bee.set_state(State::FreeFlight);
    }
  }

  pub fn build_swarm( amount: usize ) -> Vec<Bee> {
    let mut swarm: Vec<Bee> = Vec::new();
    let _rng = rand::thread_rng();

    for _i in 0..amount {
      swarm.push(Bee::new(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2,'d'));
    }
    swarm
  }
}

fn main() {
  let mut zero_hive = Hive::new(10, 20, 10, 1, (20, 30));

  zero_hive.get_colony_size();
  zero_hive.get_best_food();

  let mut main_grid_manager = GridManager::new(&zero_hive.swarm);
  main_grid_manager.print_grid();

  let root = Root::initializer()
      .font("arial10x10.png", FontLayout::Tcod)
      .font_type(FontType::Greyscale)
      .size(SCREEN_WIDTH, SCREEN_HEIGHT)
      .title("Bee stuff")
      .init();

  tcod::system::set_fps(LIMIT_FPS);

  let mut tcod = Tcod {
    root,
    con: Offscreen::new(SCREEN_WIDTH, SCREEN_HEIGHT)
  };

  let mut rogue_bee = Bee::new(20, 20, 'O');
  rogue_bee.set_state(State::Rogue);

  while !tcod.root.window_closed() {
      tcod.con.clear();

      rogue_bee.draw(&mut tcod.con);
      rogue_bee.movement(&mut main_grid_manager);

      for bee in zero_hive.swarm.iter_mut(){
        bee.draw(&mut tcod.con);
        bee.movement(&mut main_grid_manager);
      }

      blit(& tcod.con, (0, 0), (SCREEN_WIDTH, SCREEN_HEIGHT), &mut tcod.root, (0, 0), 1.0, 1.0,);

      tcod.root.flush();

      let pressed = tcod.root.wait_for_keypress(true);

      // println!("{:?}", pressed);

      match pressed.printable {
        'a' => zero_hive.attack_swarm(),
        'e' => zero_hive.ease_swarm(),
        _ => (),
      }

      if pressed.code == Escape {
          break;
      }
      main_grid_manager.print_grid();
  }
}
