# Swarm AI in Rust lang
This repository holds a small project in which i try to build a swarm
communication protocol to locate, pursue and destroy non-swarm entities.

**what is the purpose of this project?**
*I myself don't know, seems like a fun way of learning rust*.

Swarm agents are called bees, their behavior is divided into:

- *Free Flight*;
- *Pursue*;
- *Intercept*;

For non-swarm bees, the default behavior is *Rogue*.
The following section will proceed to describe each behavior.

## Free Flight ##

### Individual ###
  Passive behavioral state which translates into a non-planned, roaming
  movement pattern
  the agents have no targets neither are looking for one, they just 'fly' around
  randomly.
  Movement is implemented using a random walk algorithm.

### Collective ###
  Swarm agents have no interaction while in Free Flight, everyone just minds their
  own business.

## Pursue ##

### Individual ###
   This is an active behavioral state, is activated when a rogue agent comes in
   detection range of an idle bee.
   The pursuing agent will extrapolate each frame a different movement vector
   using its own coordinates and the rogue agent's coordinates until it has enough
   gathered enough information to calculate an interception vector in order to
   catch the hostile agent. The intercept vector is inferred through linear
   regression of the movement dataset obtained through observation of the
   targets movement patterns.

### Collective ###
   The agent which engages in a pursuit continuously broadcasts the location
   of the target. Other Swarm agents within communication range of the pursuer,
   may also lock to the target and guide themselves by the coordinates
   being transmitted, until the target is within detection range.

## Intercept ##

### Individual ###  
  The second and final active state, a swarm agent transitions into *Intercept*
  once the calculation of the intercept vector is finished. Movement in this
  state, as in *Pursue*, is aggressive with the finality of colliding with the target
  through the route given by the intercept vector.
  During *Intercept* state the swarm agent will still gather information about the target's
  movement and if for some reason the observed movement deviates from the predicted route
  the swarm agent will then regress into *Pursue* and recalculate the intercept
  vector.

### Collective ###
  The agent which engages in interception may broadcast it's prediction of the target's
  movement so that nearby pursuers may also transition into *Intercept*.

## Rogue ##

### Individual ###
  For now, linear movement just to trigger free roaming swarmers.

### Collective ###
  No collective behavior.
